#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'mrcopy'
SITENAME = u'MrcSec'
SITEURL = ''
SUBTITLE = 'Adventures in Information Security'
SITE_DESCRIPTION = 'Just another Infosec blog'

# Theme specific variables
FIRST_NAME = 'MrcSec'
#LOGO = 'logo.png'
#DEFAULT_CATEGORY = 'misc'
TWITTER_USERNAME="@mrcopy_"

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/Argentina/Buenos_Aires'
FAVICON = False
DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Menu Items
MENUITEMS = (('Home', '/'),
        ('About', '/pages/about.html'),
        ('OSCP', '/tag/oscp.html'))

# Social widget
SOCIAL = (('Twitter', 'https://twitter.com/mrcopy_'),)
TWITTER = '@mrcopy_'
GITHUB = '@mrcopy'
DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = '/data/brutalistpelican'
#THEME = '/home/mrcpy/Workspace/Development/github/brutalistpelican'
