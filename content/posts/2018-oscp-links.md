Title: OSCP Links
Date: 2018-10-06
Category: OSCP
Tags: oscp, resources
Slug: oscp-links

This is a collection of links I found useful during the OSCP certification. The majority are very technical and specific, so I'll do my best to organize them in topics. 

I'll update the post regularly.



## Enumeration/Reconnaissance

- [List of common ports](https://sushant747.gitbooks.io/total-oscp-guide/list_of_common_ports.html)
- [Enumeration Visualized](https://github.com/DigitalAftermath/EnumerationVisualized/wiki)
- [Recon my Way](https://medium.com/ehsahil/recon-my-way-82b7e5f62e21)



## Reverse Shells/Backdoors

- [Pentestmonkey cheatsheet](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet)



## Buffer Overflows

The first tutorial, from VeteranSec, I found it brilliant, and has everything that you need to know to root one OSCP machine in the exam.

- [Veteransec Tutorial](https://veteransec.com/2018/09/10/32-bit-windows-buffer-overflows-made-easy/) 



## Privilege Escalation

I'll divide this section in two: Linux and Windows.

### Linux

- [g0tmi1k's guide](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/)



### Windows

- [Andrew Smith's talk](https://www.youtube.com/watch?v=PC_iMqiuIRQ)



## Other Topics/Miscellaneous

- https://rmusser.net/docs/AnonOpsecPrivacy.html
- https://trailofbits.github.io/ctf/intro/careers.html



*Modified*:`06/10/2018 `