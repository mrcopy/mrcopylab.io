---
layout: page
title: About
permalink: /about/
---

Hi, there. I'm Lautaro Colombo, and this is my little corner on the internet.

The purpose of this blog is to organize, document and share everything I have learned and want to learn about different topics that interest me. I'll share my thoughts, ideas, notes, projects, books I read and want to read, quotes, and more things.

## Interests

I have two main interests: football and computers.

I've played football in semi-professional and professional leagues in two countries in Latin America. It has always been a passion of mine, and I still play and watch as much as I can, but not professionally for now. **My preferred position in the pitch is box-to-box midfielder**, but I also like to play as a central midfielder. I'm interested heavily on the tactics of the game, and I plan on writing about this in another blog that I will create hopefully soon.

My other main interest are computers, particularly computer security. **My main focus in that area gravitates towards Penetration Testing and Adversary Simulation, aka Red Teaming**. I am currently enrolled in PWK course to obtain OSCP to kickstart a career in the field. I've been a developer for 5+ years, and I had the opportunity to work also in different countries and with multiple people from different backgrounds.

Other activities in which I spend my time are also reading (these days mostly technical books), flamenco guitar and learn other languages (German now!).

### Contact 

The best way to contact me is through [Signal](https://signal.org/) or Whatsapp.

Also, you can shoot me an [email](mailto:lautaro@lautarocolombo.com) or found me on [Github](https://github.com/mrcopy).
